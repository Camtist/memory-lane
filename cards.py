
class Card:
    def __init__(self, own, channel, lane):
        self.owner = own
        self.name = ''
        self.effect = ''
        self.location = None
        self.is_face_down = None
        self.is_active = None
        self.actives = []
        self.dream = None
        self.channel = channel
        self.mods = ''
        self.peeked = {}
        self.lane = lane
        self.has_flipped = False
    
    def display(self):
        if self.mods:
            return self.name + ' (' + self.mods + ')'
        else:
            return self.name

    async def on_exist(self):
        self.location = 'deck'

    async def on_play(self, is_face_down, is_active, dream=None):
        self.location = 'field'
        self.is_face_down = is_face_down
        self.is_active = is_active
        self.dream = dream
        if dream is not None:
            dream.actives.append(self)

    async def on_round_start(self):
        for peeker in list(self.peeked.keys()):
            if self.owner.active_player:
                self.peeked[peeker][1] += 1

    async def on_round_end(self):
        self.has_flipped = False

    async def on_remove(self):
        self.owner.discard.append(self)
        self.location = 'discard'
        if self.is_active:
            self.dream.actives.remove(self)
            self.dream = None
        else:
            for active in self.actives:
                await active.on_remove()
            self.actives = []
        self.mods = ''
        self.peeked = {}
        self.is_face_down = None
        self.is_active = None
        await self.channel.send(self.name + ' has been discarded.')

    async def on_flip(self):
        if self.is_face_down:
            await self.on_reveal()
        else:
            await self.on_hide()

    async def on_reveal(self):
        self.is_face_down = False
        self.has_flipped = True

    async def on_hide(self):
        self.is_face_down = True
        self.has_flipped = True

    async def on_add_active(self):
        pass

    async def update(self):
        pass

    async def on_peek(self, who):
        self.peeked[who.id] = [self.display(), 0]
        await who.user.send('You see ' + self.name + '.')


class Dream(Card):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == True:
            await self.channel.send(self.name + ' has been revealed as an Active despite being a Dream.')
            await self.on_remove()
            return


class Friend(Card):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [0, 0, 0]
        self.cost = 0
        self.red = 0
        self.blue = 0
        self.yellow = 0
        self.types = []

    async def on_play(self, is_face_down, is_active, dream=None):
        await super().on_play(is_face_down, is_active, dream)
        self.owner.value -= self.cost
        await self.owner.user.send(self.name + " cost " + str(self.cost) + ". You have " + str(self.owner.value) + " Value remaining.")

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.channel.send(self.name + ' has been revealed as a Passive despite being a Friend.')
            await self.on_remove()
            return

    async def update_mod(self):
        self.mods = ''
        if self.red != self.base[0]:
            if self.red > self.base[0]:
                self.mods += '+' + str(self.red - self.base[0]) + 'R '
            else:
                self.mods += '-' + str(self.base[0] - self.red) + 'R '
        if self.blue != self.base[1]:
            if self.blue > self.base[1]:
                self.mods += '+' + str(self.blue - self.base[1]) + 'B '
            else:
                self.mods += '-' + str(self.base[1] - self.blue) + 'B '
        if self.yellow != self.base[2]:
            if self.yellow > self.base[2]:
                self.mods += '+' + str(self.yellow - self.base[2]) + 'Y '
            else:
                self.mods += '-' + str(self.base[2] - self.yellow) + 'Y '
        if self.mods:
            self.mods = self.mods[:-1]


class Memento(Card):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.cost = 0

    async def on_play(self, is_face_down, is_active, dream=None):
        await super().on_play(is_face_down, is_active, dream)
        self.owner.value -= self.cost
        await self.owner.user.send(self.name + " cost " + str(self.cost) + ". You have " + str(self.owner.value) + " Value remaining.")

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.channel.send(self.name + ' has been revealed as a Passive despite being a Memento.')
            await self.on_remove()
            return


# --- Dreams ---


class Blank_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Blank Room"
        self.effect = "-"


class The_Void(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 3
        self.name = "The Void"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('The Void reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    await self.channel.send('The Void destroys ' + active.name + '.')
                    await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("The Void rests.")


class Red_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 2
        self.name = "Red Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest Red stat."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestRed = max([a.red for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Red Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if active.red < highestRed:
                        await self.channel.send('Red Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Red Room rests.")


class Blue_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 2
        self.name = "Blue Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest Blue stat."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestBlue = max([a.blue for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Blue Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if active.blue < highestBlue:
                        await self.channel.send('Blue Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Blue Room rests.")


class Yellow_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 2
        self.name = "Yellow Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest Yellow stat."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestYellow = max([a.yellow for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Yellow Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if active.yellow < highestYellow:
                        await self.channel.send('Yellow Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Yellow Room rests.")

class Purple_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 2
        self.name = "Purple Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest summed red and blue stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestSum = max([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Purple Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.red + active.blue) < highestSum:
                        await self.channel.send('Purple Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Purple Room rests.")


class Green_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 2
        self.name = "Green Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest summed blue and yellow stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestSum = max([(a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Green Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.blue + active.yellow) < highestSum:
                        await self.channel.send('Green Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Green Room rests.")


class Orange_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 2
        self.name = "Orange Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest summed red and yellow stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestSum = max([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Orange Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.red + active.yellow) < highestSum:
                        await self.channel.send('Orange Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Orange Room rests.")


class Dark_Room(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 3
        self.name = "Dark Room"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the highest summed stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            highestSum = max([(a.red + a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Dark Room reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.red + active.blue + active.yellow) < highestSum:
                        await self.channel.send('Dark Room destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Dark Room rests.")


class Red_Pit(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -2
        self.name = "Red Pit"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest Red stat."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestRed = min([a.red for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Red Pit reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if active.red > lowestRed:
                        await self.channel.send('Red Pit destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Red Pit rests.")


class Blue_Pit(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -2
        self.name = "Blue Pit"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest Blue stat."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestBlue = min([a.blue for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Blue Pit reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if active.blue > lowestBlue:
                        await self.channel.send('Blue Pit destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Blue Pit rests.")


class Yellow_Pit(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -2
        self.name = "Yellow Pit"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest Yellow stat."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestYellow = min([a.yellow for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Yellow Pit reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if active.yellow > lowestYellow:
                        await self.channel.send('Yellow Pit destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Yellow Pit rests.")


class Purple_Pit(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -2
        self.name = "Purple Pit"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest summed red and blue stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestSum = min([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Purple Pit reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.red + active.blue) > lowestSum:
                        await self.channel.send('Purple Pit destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Purple Pit rests.")


class Green_Pit(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -2
        self.name = "Green Pit"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest summed blue and yellow stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestSum = min([(a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Green Pit reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.blue + active.yellow) > lowestSum:
                        await self.channel.send('Green Pit destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Green Pit rests.")


class Orange_Pit(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -2
        self.name = "Orange Pit"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest summed red and yellow stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestSum = min([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Orange Pit reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.red + active.yellow) > lowestSum:
                        await self.channel.send('Orange Pit destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Orange Pit rests.")


class Black_Hole(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = -3
        self.name = "Black Hole"
        self.effect = "When revealed or while revealed, reveal all Actives here. Remove all Friends save for those with the lowest summed stats."

    async def on_reveal(self):
        await super().on_reveal()
        if self.is_active == False:
            await self.update()
        
    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if self.is_face_down == False and len(self.actives) > 0:
            lowestSum = min([(a.red + a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if active.is_face_down == True:
                    await self.channel.send('Black Hole reveals ' + active.name + '.')
                    await active.on_reveal()
                i += 1 - (length_check - len(self.actives))
            i = 0
            while i < len(self.actives):
                length_check = len(self.actives)
                active = self.actives[i]
                if isinstance(active, Friend):
                    if (active.red + active.blue + active.yellow) > lowestSum:
                        await self.channel.send('Black Hole destroys ' + active.name + '.')
                        await active.on_remove()
                i += 1 - (length_check - len(self.actives))
            await self.channel.send("Black Hole rests.")


class Red_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Red Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest Red stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highestRed = max([a.red for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highestRed
            await self.channel.send("Red Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highestRed) + ".")


class Blue_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Blue Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest Blue stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highestBlue = max([a.blue for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highestBlue
            await self.channel.send("Blue Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highestBlue) + ".")


class Yellow_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Yellow Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest Yellow stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highestYellow = max([a.yellow for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highestYellow
            await self.channel.send("Yellow Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highestYellow) + ".")


class Purple_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Purple Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest sum of Red and Blue stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highestPurple = max([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highestPurple
            await self.channel.send("Purple Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highestPurple) + ".")


class Green_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Green Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest sum of Blue and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highestGreen = max([(a.blue + a.red) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highestGreen
            await self.channel.send("Green Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highestGreen) + ".")


class Orange_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Orange Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest sum of Red and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highestOrange = max([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highestOrange
            await self.channel.send("Orange Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highestOrange) + ".")


class Night_Sky(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Night Sky"
        self.effect = "When revealed, reduce the Value of the Active Player by the highest sum of all stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value -= highest
            await self.channel.send("Night Sky reduces " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Red_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Red Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest Red stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Red Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Blue_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Blue Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest Blue stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Blue Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Yellow_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Yellow Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest Yellow stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Yellow Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Purple_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Purple Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest sum of Red and Blue stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Purple Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Green_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Green Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest sum of Blue and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Green Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Orange_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Orange Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest sum of Red and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Orange Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Dark_Mine(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Dark Mine"
        self.effect = "When revealed, reduce the Value of the Inactive Player by the highest sum of all stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value -= highest
            await self.channel.send("Dark Mine reduces " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Red_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Red Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest Red stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Red Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Blue_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Blue Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest Blue stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Blue Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Yellow_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Yellow Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest Yellow stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Yellow Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Purple_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Purple Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest sum of Red and Blue stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Purple Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Green_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Green Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest sum of Blue and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Green Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Orange_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Orange Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest sum of Red and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Orange Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Dark_Factory(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Dark Factory"
        self.effect = "When revealed, increase the Value of the Inactive Player by the highest sum of all stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.inactive_player.value += highest
            await self.channel.send("Dark Factory increases " + self.lane.inactive_player.name + "'s Value by " + str(highest) + ".")


class Red_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Red Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest Red stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Red Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Blue_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Blue Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest Blue stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Blue Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Yellow_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Yellow Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest Yellow stat among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Yellow Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Purple_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Purple Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest sum of Red and Blue stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Purple Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Green_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Green Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest sum of Blue and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Green Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Orange_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Orange Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest sum of Red and Yellow stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Orange Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Dark_Field(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 1
        self.name = "Dark Field"
        self.effect = "When revealed, increase the Value of the Active Player by the highest sum of all stats among this dream's Friends."

    async def on_reveal(self):
        await super().on_reveal()
        if len(self.actives) > 0:
            highest = max([(a.red + a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
            self.lane.active_player.value += highest
            await self.channel.send("Dark Field increases " + self.lane.active_player.name + "'s Value by " + str(highest) + ".")


class Red_Tower(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0
        self.name = "Red Tower"
        self.effect = "Value is equal to the highest Red stat among its Actives."

    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if len([a for a in self.actives if isinstance(a, Friend) ]) > 0:
            self.value = max([(a.red) for a in self.actives if isinstance(a, Friend)])
        else:
            self.value = 0


class Blue_Tower(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0
        self.name = "Blue Tower"
        self.effect = "Value is equal to the highest Blue stat among its Actives."

    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if len([a for a in self.actives if isinstance(a, Friend) ]) > 0:
            self.value = max([(a.blue) for a in self.actives if isinstance(a, Friend)])
        else:
            self.value = 0


class Yellow_Tower(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0
        self.name = "Yellow Tower"
        self.effect = "Value is equal to the highest Yellow stat among its Actives."

    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if len([a for a in self.actives if isinstance(a, Friend) ]) > 0:
            self.value = max([(a.yellow) for a in self.actives if isinstance(a, Friend)])
        else:
            self.value = 0


class Purple_Tower(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0
        self.name = "Purple Tower"
        self.effect = "Value is equal to the highest sum of Red and Blue stats among its Actives."

    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if len([a for a in self.actives if isinstance(a, Friend) ]) > 0:
            self.value = max([(a.red + a.blue) for a in self.actives if isinstance(a, Friend)])
        else:
            self.value = 0


class Green_Tower(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0
        self.name = "Green Tower"
        self.effect = "Value is equal to the highest sum of Blue and Yellow stats among its Actives."

    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if len([a for a in self.actives if isinstance(a, Friend) ]) > 0:
            self.value = max([(a.blue + a.yellow) for a in self.actives if isinstance(a, Friend)])
        else:
            self.value = 0


class Orange_Tower(Dream):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.value = 0
        self.name = "Orange Tower"
        self.effect = "Value is equal to the highest sum of Red and Yellow stats among its Actives."

    async def on_add_active(self):
        await super().on_add_active()
        await self.update()

    async def update(self):
        await super().update()
        if len([a for a in self.actives if isinstance(a, Friend) ]) > 0:
            self.value = max([(a.red + a.yellow) for a in self.actives if isinstance(a, Friend)])
        else:
            self.value = 0


# --- Friends ---


class Young_Kathy(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [2, 4, 3]
        self.cost = 3
        self.red = 2
        self.blue = 4
        self.yellow = 3
        self.name = "Young Kathy"
        self.effect = "-"
        self.types.append("Child")


class Young_Will(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [3, 3, 3]
        self.cost = 3
        self.red = 3
        self.blue = 3
        self.yellow = 3
        self.name = "Young Will"
        self.effect = "-"
        self.types.append("Child")


class Knife_Clasped_Cloak(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [1, 3, 1]
        self.cost = 1
        self.red = 1
        self.blue = 3
        self.yellow = 1
        self.name = "Knife Clasped Cloak"
        self.effect = "When revealed, the active player may hide a revealed card."
    
    async def on_reveal(self):
        await super().on_reveal()
        choice = self.lane.active_player.name + ' may hide a card on the field. Submit via ~choose.'
        await self.channel.send(choice)
        self.lane.active_player.choose_queue.append([choice, True, lambda card: not card.is_face_down, lambda card: card.on_hide(), lambda card: self.channel.send("Knife Clasped Cloak hides " + card.display() + ".")])


class Wheel_Of_Eyes(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [2, 4, 4]
        self.cost = 5
        self.red = 2
        self.blue = 4
        self.yellow = 4
        self.name = 'Wheel of Eyes'
        self.effect = 'If hidden, at the start of each turn, its owner may peek at a hidden card. If revealed, at the start of each turn, its owner may reveal a hidden card.'
    
    async def on_round_start(self):
        await super().on_round_start()
        if self.owner.active_player:
            if self.is_face_down:
                choice = 'You may peek at a card on the field. Submit via ~choose.'
                await self.owner.user.send(choice)
                self.owner.choose_queue.append([choice, True, lambda card: card.is_face_down, lambda card: card.on_peek(self.owner)])
            else:
                choice = 'You may reveal a card on the field. Submit via ~choose.'
                await self.owner.user.send(choice)
                self.owner.choose_queue.append([choice, True, lambda card: card.is_face_down, lambda card: self.channel.send(card.display() + ' has been revealed.'), lambda card: card.on_reveal()])


class Mother_May_I(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [4, 3, 3]
        self.cost = 3
        self.red = 4
        self.blue = 3
        self.yellow = 3
        self.name = 'Mother May I'
        self.effect = 'Raise the Yellow stat of all Children on the same Dream at the end of its owner\'s turn.'
    
    async def on_round_end(self):
        await super().on_round_end()
        if self.owner.active_player:
            if self.dream is not None:
                for active in self.dream.actives:
                    if 'Child' in active.types:
                        active.yellow += 1
                        await active.update_mod()


class Old_Penelope(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [1, 5, 4]
        self.cost = 4
        self.red = 1
        self.blue = 5
        self.yellow = 4
        self.types.append('Elder')
        self.name = 'Old Penelope'
        self.effect = '-'


class Old_Gerard(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [2, 4, 4]
        self.cost = 4
        self.red = 2
        self.blue = 4
        self.yellow = 4
        self.types.append('Elder')
        self.name = 'Old Gerard'
        self.effect = '-'


class Father_Dearest(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [3, 4, 3]
        self.cost = 4
        self.red = 3
        self.blue = 4
        self.yellow = 3
        self.name = 'Father Dearest'
        self.effect = 'When revealed, the active player may remove an Active from any Dream.'

    async def on_reveal(self):
        await super().on_reveal()
        choice = self.lane.active_player.name + ' may remove an Active on any Dream. Submit via ~choose.'
        await self.channel.send(choice)
        self.lane.active_player.choose_queue.append([choice, True, lambda card: bool(card.dream), lambda card: card.on_remove(), lambda card: self.channel.send('Father Dearest removes ' + ('a Hidden Active' if card.is_face_down else card.display()) + ".")])


class Debtor(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [3, 3, 3]
        self.cost = -3
        self.red = 3
        self.blue = 3
        self.yellow = 3
        self.name = 'Debtor'
        self.effect = 'When revealed, this Friend immediately allies with its owner\'s opponent, becoming their\'s instead.'

    async def on_reveal(self):
        await super().on_reveal()
        self.owner = self.owner.opponent
        await self.channel.send('The Debtor allies themselves to ' + self.owner.name + '.')


class Of_Reversed_Wings(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [1, 2, 5]
        self.cost = 5
        self.red = 1
        self.blue = 2
        self.yellow = 5
        self.name = 'Of Reversed Wings'
        self.effect = 'Owner gains 1 Value at the start and end of each turn.'

    async def on_round_start(self):
        await super().on_round_start()
        self.owner.value += 1
        await self.owner.user.send('Of Reversed Wings gives you 1 Value.')

    async def on_round_end(self):
        await super().on_round_end()
        self.owner.value += 1
        await self.owner.user.send('Of Reversed Wings gives you 1 Value.')


class Unjust_Goat(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [3, 1, 3]
        self.cost = 2
        self.red = 3
        self.blue = 1
        self.yellow = 3
        self.name = 'Unjust Goat'
        self.effect = 'When revealed, the active player gains 2 Value and their opponent loses 2 Value.'

    async def on_reveal(self):
        await super().on_reveal()
        self.lane.active_player.value += 2
        self.lane.active_player.opponent.value -= 2
        await self.channel.send('The Unjust Goat grants 2 Value to ' + self.lane.active_player.name + ' and subtracts 2 Value from ' + self.lane.active_player.opponent.name)


class Cutest_Kitten(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [1, 1, 2]
        self.cost = 0
        self.red = 1
        self.blue = 1
        self.yellow = 2
        self.name = 'Cutest Kitten'
        self.effect = 'If removed, the active player loses 3 Value. Reveal if about to be removed.'

    async def on_remove(self):
        if self.is_face_down:
            await self.channel('The Cutest Kitten reveals itself!')
            self.on_reveal()
        target = self.owner if self.owner.active_player else self.owner.opponent
        target.value -= 3
        await self.channel('The Cutest Kitten takes 3 Value from ' + target.name + '.')
        await super().on_remove()


class Bobber_Raron(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [3, 1, 2]
        self.cost = 3
        self.red = 3
        self.blue = 1
        self.yellow = 2
        self.name = 'Bobber Raron'
        self.effect = 'When revealed, active player loses 3 Value.'

    async def on_reveal(self):
        await super().on_reveal()
        self.lane.active_player.value -= 3
        await self.channel.send('Bobber Raron lowers ' + self.active_player.name + '\'s Value by 3.')


class A_Man(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [2, 2, 2]
        self.cost = 2
        self.red = 2
        self.blue = 2
        self.yellow = 2
        self.name = 'A Man'
        self.effect = '-'


class Not_A_Man(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [1, 1, 1]
        self.cost = 1
        self.red = 1
        self.blue = 1
        self.yellow = 1
        self.name = 'Not A Man'
        self.effect = 'When revealed, if A Man is on the same dream (even if face-down), discard all instances A Man on the dream and gain +2 to all stats for each discarded.'

    async def on_reveal(self):
        await super().on_reveal()
        i = 0
        for card in self.dream.actives:
            if card.name == 'A Man':
                i += 1
                card.on_remove()
        if i != 0:
            self.red += 2 * i
            self.blue += 2 * i
            self.yellow += 2 * i
            self.update_mod()
            await self.channel.send('Not A Man absorbs the power of ' + str(i) + ' Mans.')


class Looky_Lou(Friend):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.base = [1, 4, 1]
        self.cost = 3
        self.red = 1
        self.blue = 4
        self.yellow = 1
        self.name = 'Looky Lou'
        self.effect = 'When revealed, the active player may peek at another card.'
        self.types.append("Child")

    async def on_reveal(self):
        await super().on_reveal()
        choice = self.lane.active_player.name + ' may peek at a card on the field. Submit via ~choose.'
        await self.channel.send(choice)
        self.lane.active_player.choose_queue.append([choice, True, lambda card: card.is_face_down, lambda card: card.on_peek(self.lane.active_player)])

# --- Mementos ---


class Folly(Memento):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.cost = 0
        self.name = 'Folly'
        self.effect = '-'


class Why(Memento):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.cost = 1
        self.name = 'Why'
        self.effect = '-'


class But_Why(Memento):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.cost = 2
        self.name = 'But Why'
        self.effect = '-'


class Batta_Ri(Memento):
    def __init__(self, own, channel, lane):
        super().__init__(own, channel, lane)
        self.cost = 1
        self.value = 0
        self.name = 'Batta Ri'
        self.effect = 'While on the field, builds in Value by 1 at the start of its owner\'s turn. When revealed, grants this Value to the active player.'

    async def on_round_start(self):
        await super().on_round_start()
        if self.owner.active_player:
            if self.is_face_down:
                self.value += 1
                self.mods = str(self.value)
    
    async def on_reveal(self):
        await super().on_reveal()
        target = self.owner if self.owner.active_player else self.owner.opponent
        target += self.value
        await self.channel('Batta Ri gives ' + str(self.value) + ' Value to ' + target.name + '.')
        self.value = 0
        self.mods = '0'
