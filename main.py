#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import discord
import git
import sys
import traceback
from discord.ext import commands

import memorylane


path_here = os.path.dirname(os.path.realpath(__file__))
os.chdir(path_here)


class myHelp(commands.DefaultHelpCommand):

    async def on_help_command_error(self, ctx, error):
        await ctx.send('Error sending help message. You must allow messages from server members to receive the help message.')

    def get_ending_note(self):
        return '```Type `%help <command>` for more info on a command.\nYou can also type `%help <category>` for more info on a category.```'


b = commands.Bot(command_prefix=('~'), case_insensitive=True, help_command=myHelp(dm_help=None))

@b.command()
async def hi(ctx, *args):
    '''The hi command. I'll greet the user.
    '''
    await ctx.send('Hi, <@' + str(ctx.author.id) + '>!')

@b.command()
async def update(ctx, *args):
    '''Updates the bot.'''
    repo = git.Repo('.')
    repo.remotes.origin.fetch()
    repo.git.reset('--hard','origin/master')
    master = repo.head.reference
    message = master.commit.message.split("\n")[0]
    await ctx.send(f'Updated to commit *{message}*. Restarting!')
    await ctx.bot.logout()
    sys.exit(1)

@b.event
async def on_command_error(ctx, error):
    if ctx.message.guild and type(error) == discord.ext.commands.errors.CommandInvokeError:
        log_chan_cat = [cat for cat in ctx.message.guild.categories if cat.name.lower() == "bot-dev"][0]
        log_chan = [chan for chan in log_chan_cat.channels if chan.name.lower() == "errors"][0]
        escaped_message = ctx.message.content.replace("`", "<backtick>")
        cmd = f'<#{ctx.message.channel.id}> **{ctx.author.display_name}**: `{escaped_message}`'
        tb = ''.join(traceback.format_tb(error.original.__traceback__))
        error = discord.utils.escape_markdown(f'{repr(error.original)}')
        await log_chan.send(f'{error} in command:\n{cmd}\n```{tb}```')

b.add_cog(memorylane.MemoryLane())

with open('secret') as s:
    token = s.read()[:-1]
# Read the Discord bot token from a secret file

b.run(token)
# Start the bot, finally!