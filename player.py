import random

class Player:
    def __init__(self, name, id, user, channel):
        self.name = name
        self.id = id
        self.user = user
        self.deck = None
        self.hand = []
        self.discard = []
        self.value = 20
        self.active_player = False
        self.spending = 0
        self.choose_queue = []
        self.opponent = None
        self.channel = channel

    def draw(self, num):
        if num > len(self.deck):
            module = __import__('cards')
            _class = getattr(module, 'The_Blank_Room')
            for x in range(num - len(self.deck)):
                self.deck.append(_class(self, self.channel))
        drawn_cards = random.sample(self.deck, num)
        for draw in drawn_cards:
            self.hand.append(draw)
            self.deck.remove(draw)
