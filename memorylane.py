import discord, os
from discord.ext import commands
import cards, player, database
import random, json

registry = {}

class Lane():
    def __init__(self, id, player1, player2, channel):
        super().__init__()
        self.id = id
        self.players = [player1, player2]
        self._field = []
        self.active_player = None
        self.inactive_player = None
        self.in_game = False
        self.channel = channel


    async def addtesty(self, ctx, *args):
        testy = player.Player("testy", "123", ctx.author, self.channel)
        deck = []
        for x in range(20):
            card = cards.Blank_Room(testy, self.channel, self)
            deck.append(card)
        testy.deck = deck
        self.players[1] = testy
        await self.channel.send("Added Testy.")


    async def challenge(self, ctx):
        for player in self.players:
            if player.id != ctx.author.id:
                if player.value <= 0:
                    return True
        return False


    async def choose(self, ctx, *args):
            if self.in_game == False:
                await ctx.send('The game has not begun.')
                return
            elif self.active_player.id != ctx.author.id:
                await ctx.send('It is not your turn.')
                return
            elif not self.active_player.choose_queue:
                await ctx.send('You have no choices to make.')
                return

            for p in self.players:
                if p is not None and ctx.author.id == p.id:
                    if len(args) == 0:
                        await ctx.send(p.choose_queue[0][0])
                        return
                    elif args[0].isdigit():
                        if args[0] == 0:
                            if p.choose_queue[0][1]:
                                p.choose_queue.pop(0)
                                await ctx.send("Choice skipped.")
                                return
                            else:
                                await ctx.send("Choice may not be skipped.")
                                return
                        card = registry[ctx.author.id]._field[int(args[0]) - 1]
                        if len(args) > 1:
                            if args[1].isdigit():
                                card = card.actives[int(args[1]) - 1]
                        if p.choose_queue[0][2](card):
                            for x in range(3, len(p.choose_queue[0])):
                                await p.choose_queue[0][x](card)
                            p.choose_queue.pop(0)
                        else:
                            await ctx.send("Invalid card.")
                            return

    async def damage(self, num):
        if num == 0:
            return
        for player in self.players:
            if player.id != self.active_player.id:
                player.value -= num
                await player.user.send("You have taken " + str(num) + " damage. You have " + str(player.value) + " remaining.")


    async def die(self, ctx):
        await self.channel.send(ctx.author.display_name + " has won the match. Thank you for playing.")
        for player in self.players:
            del registry[player.id]


    async def discard(self, ctx):
        if self.in_game == False:
            await ctx.send('The game has not begun.')
            return

        for p in self.players:
            if p is not None and ctx.author.id == p.id:
                msg = 'Your discard pile: '
                for card in p.discard:
                    msg += card.display() + ', '
                if len(p.discard) == 0:
                    msg += '[Empty], '
                msg = msg[:-2]
                await ctx.author.send(msg)


    async def end(self, ctx, *args, bypass=False):
        if self.in_game == False:
            await ctx.send('The game has not begun.')
            return
        elif self.active_player.id != ctx.author.id and not bypass:
            await ctx.send('It is not your turn.')
            return
        elif self.active_player.choose_queue:
            await ctx.send('You still have choices to make.')
            return

        self.active_player.spending = 0
        for dream in self._field:
            await dream.on_round_end()
            for active in dream.actives:
                if active.owner.id == self.active_player.id:
                    await active.on_round_end()
        await self.channel.send(self.active_player.name + ' has ended their turn.')

        self.inactive_player = self.active_player

        for player in self.players:
            if player.active_player == False:
                self.active_player = player
            player.active_player = not player.active_player

        self.active_player.draw(1)

        for dream in self._field:
            await dream.on_round_start()
            for active in dream.actives:
                if active.owner.id == self.active_player.id:
                    await active.on_round_start()

        tempVal = 0
        for dream in self._field:
            if hasattr(dream, 'value'):
                count = 0
                for active in dream.actives:
                    if active.owner.id == self.active_player.id:
                        count += 1
                if count > int(len(dream.actives) / 2):
                    tempVal += dream.value
        if tempVal > 0:
            await self.active_player.user.send("You have gained " + str(tempVal) + " Value. Use the command ~spend [heal] [damage] to spend it.")
            self.active_player.spending = tempVal

        self.active_player.has_flip = True

        await self.channel.send("It is now " + self.active_player.name + "\'s turn.")

        if self.active_player.id == "123":
            await self.end(ctx, bypass=True)


    async def field(self, ctx, *args):
        if isinstance(ctx.channel, discord.TextChannel):
            if self._field == []:
                await ctx.send('There are no cards on the field.')
            else:
                msg = '__Current Field__\n'
                for i, c in enumerate(self._field):
                    if c.is_face_down == True:
                        msg += str(i + 1) + '. Hidden Dream: ' + str(len(c.actives)) + " Active - "
                    else:
                        msg += str(i + 1) + '. ' + c.display() + ': ' + str(len(c.actives)) + " Active - "
                    for a in c.actives:
                        if a.is_face_down == True:
                            msg += 'Hidden Active, '
                        else:
                            msg += a.display() + ', '
                    if len(c.actives) > 0:
                        msg = msg[:-2]
                    else:
                        msg = msg[:-3]
                    msg += '\n'

                await ctx.send(msg)

                msg = '__Current Field__\n'
                for i, c in enumerate(self._field):
                    if c.is_face_down == True:
                        if c.owner.id == ctx.author.id:
                            msg += str(i + 1) + '. [' + c.display() + ']: ' + str(len(c.actives)) + " Active - "
                        elif ctx.author.id in c.peeked:
                            msg += str(i + 1) + '. {' + c.peeked[ctx.author.id][0] + ' | ' + str(c.peeked[ctx.author.id][1]) + ' turns ago}: ' + str(len(c.actives)) + " Active - "
                        else:
                            msg += str(i + 1) + '. Hidden Dream: ' + str(len(c.actives)) + " Active - "
                    else:
                        msg += str(i + 1) + '. ' + c.display() + ': ' + str(len(c.actives)) + " Active - "
                    for a in c.actives:
                        if a.is_face_down == True:
                            if a.owner.id == ctx.author.id:
                                msg += '[' + a.display() + '], '
                            elif ctx.author.id in a.peeked:
                                msg += '{' + a.peeked[ctx.author.id][0] + ' | ' + str(a.peeked[ctx.author.id][1]) + ' turns ago}, '
                            else:
                                msg += 'Hidden Active, '
                        else:
                            msg += a.display() + ', '
                    if len(c.actives) > 0:
                        msg = msg[:-2]
                    else:
                        msg = msg[:-3]
                    msg += '\n'
                
                await ctx.author.send(msg)


    async def flip(self, ctx, *args):
        if self.in_game == False:
            await ctx.send('The game has not begun.')
            return
        elif self.active_player.id != ctx.author.id:
            await ctx.send('It is not your turn.')
            return

        if len(args) > 0:
            if args[0].isdigit():
                card = self._field[int(args[0]) - 1]
                if len(args) > 1:
                    if args[1].isdigit():
                        card = card.actives[int(args[1]) - 1]
                if card.has_flipped:
                    await ctx.send('This card has already flipped at least once this turn. It cannot be manually flipped again.')
                    return
                if card.is_face_down == True:
                    await self.channel.send(self.active_player.name + ' revealed ' + card.display() + ' on ' + ('field' if card.dream is None else 'Hidden Dream' if card.dream.is_face_down else card.dream.display()) + '.')
                    await card.on_reveal()
                    if card.dream is not None:
                        await card.dream.update()
                else:
                    await self.channel.send(self.active_player.name + ' hid ' + card.display() + ' on ' + ('field' if card.dream is None else 'Hidden Dream' if card.dream.is_face_down else card.dream.display()) + '.')
                    await card.on_hide()
                    if card.dream is not None:
                        await card.dream.update()


    async def hand(self, ctx, *args):
        for p in self.players:
            if p is not None and ctx.author.id == p.id:
                msg = 'Current Hand: '
                for card in p.hand:
                    msg += card.display() + ', '
                if len(p.hand) == 0:
                    msg += '[Empty], '
                msg = msg[:-2]
                await ctx.author.send(msg)


    async def play(self, ctx, *args):
        if self.in_game == False:
            await ctx.send('The game has not begun.')
            return
        elif self.active_player.id != ctx.author.id:
            await ctx.send('It is not your turn.')
            return
        p = self.active_player
        args = [a.lower() for a in args]
        card = None
        face_down = None
        active = None
        for c in p.hand:
            if all(x in args for x in c.name.lower().split()):
                card = c
                break
        if 'u' in args:
            face_down = False
        if 'd' in args:
            face_down = True
        if 'a' in args:
            active = True
        if 'p' in args:
            active = False
        
        if card is None or face_down is None or active is None:
            if card is None:
                await ctx.send('Card not found in hand.')
            elif face_down is None:
                await ctx.send('Please specify face-up or face-down with arguments \'u\' or \'d\'.')
            elif active is None:
                await ctx.send('Please specify active or passive with arguments \'a\' or \'p\'.')
            return

        if active:
            arg_nums = [int(a) for a in args if a.isdigit()]
            passive = None
            if len(arg_nums) != 0 and arg_nums[0] > 0 and arg_nums[0] <= len(self._field):
                passive = self._field[arg_nums[0] - 1]
                await card.on_play(face_down, active, passive)
                await ctx.send('Played ' + card.display() + (' face-down' if face_down else ' face-up') + ' onto Dream ' + str(arg_nums[0]) + '.')
                await self.channel.send(self.active_player.name + ' played ' + ('a card' if face_down else card.name) + (' face-down' if face_down else ' face-up') + ' onto Dream ' + str(arg_nums[0]) + '.')
                p.hand.remove(card)
                await card.dream.on_add_active()
            else:
                await ctx.send('Please specify which Dream to place this active on by including the Dream\'s number in your arguments.')
        else:
            await card.on_play(face_down, active)
            self._field.append(card)
            await ctx.send('Played ' + card.display() + (' face-down' if face_down else ' face-up') + ' onto the field.')
            await self.channel.send(self.active_player.name + ' played ' + ('a card' if face_down else card.name) + (' face-down' if face_down else ' face-up') + ' onto the field.')
            p.hand.remove(card)


    async def spend(self, ctx, *args):
        if self.in_game == False:
            await ctx.send('The game has not begun.')
            return
        elif self.active_player.id != ctx.author.id:
            await ctx.send('It is not your turn.')
            return
        elif self.active_player.spending == 0:
            await ctx.send('You have no Value to spend.')
            return

        if len(args) < 2:
            await ctx.send('Two values are required.')
            return
        if args[0].isdigit() and args[1].isdigit():
            if int(args[0]) + int(args[1]) == self.active_player.spending:
                self.active_player.spending = 0
                self.active_player.value += int(args[0])
                await ctx.send("You have gained " + args[0] + " Value.")
                await self.damage(int(args[1]))
                await ctx.send("You have reduced your opponent's Value by " + args[1] + ".")


    async def start(self, ctx, *args):
        if self.in_game:
            await ctx.send("The game is already in progress.")
            return
        if None in self.players:
            await ctx.send("Two players are required to begin.")
            return
        await self.channel.send("Starting game...")
        for p1 in self.players:
            for p2 in self.players:
                if p1.id != p2.id:
                    p1.opponent = p2
        for player in self.players:
            for card in player.deck:
                await card.on_exist()
        self.active_player = random.choice(self.players)
        self.active_player.active_player = True
        for p in self.players:
            if p.active_player == False:
                self.inactive_player = p
        self.active_player.has_flip = True
        await self.channel.send(self.active_player.name + " goes first. Drawing hands...")
        for player in self.players:
            player.draw(5 if player.active_player else 6)
            msg = 'Current Hand: '
            for card in player.hand:
                msg += card.name + ', '
            if len(player.hand) == 0:
                msg += '[Empty], '
            msg = msg[:-2]
            await player.user.send(msg)
        self.in_game = True
        await self.channel.send("Begin!")
        
        if self.active_player.id == "123":
            await self.end(ctx, bypass=True)


    async def status(self, ctx, *args):
        for p in self.players:
            if p is not None and ctx.author.id == p.id:
                await ctx.author.send('---')
                await self.field(ctx, *args)
                await self.value(ctx, *args)
                await self.hand(ctx, *args)
                await ctx.author.send('---')


    async def value(self, ctx, *args):
        for p in self.players:
            if p is not None and ctx.author.id == p.id:
                msg = 'Current Value: ' + str(p.value)
                await ctx.author.send(msg)


class MemoryLane(commands.Cog):
    def __init__(self):
        super().__init__()
        self.database = database.Database()
        self.lanes = []


    @commands.command()
    async def addtesty(self, ctx, *args):
        '''Adds a very dumb test bot.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].addtesty(ctx, *args)


    @commands.command()
    async def challenge(self, ctx, *args):
        '''Challenges the opponent's Value.
        User wins if their opponent's Value is 0.
        User loses 1 Value if theiropponent's Value is not 0.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        elif registry[ctx.author.id].in_game == False:
            await ctx.send('The game has not begun.')
            return
        check = await registry[ctx.author.id].challenge(ctx)
        if check:
            await ctx.send("Correct! You win!")
            self.lanes.remove(registry[ctx.author.id])
            await registry[ctx.author.id].die(ctx)
        else:
            await ctx.send("Incorrect, your opponent still has Value. Your own Value has been reduced by 1.")
            for player in registry[ctx.author.id].players:
                if player.id == ctx.author.id:
                    player.value -= 1


    @commands.command()
    async def choose(self, ctx, *args):
        '''Makes a choice, depending on the card that requests it.
        Format depends on the card, but tends to be the same as ~flip.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].choose(ctx, *args)


    @commands.command()
    async def decks(self, ctx, cmd, *args):
        '''TBD'''
        f = open('decks.json')
        data = json.load(f)
        f.close()
        changeFlag = False

        if ctx.author.id not in data.keys():
            changeFlag = True
            data[ctx.author.id] = {
                'random': random.choices([key.replace(' ', '_') for key in list(self.database.data.keys())], k=30),
            }

        if cmd.lower() == 'show':
            if len(args) == 0:
                msg = ctx.author.display_name + '\'s Decks: '
                for deckName in list(data[ctx.author.id].keys()):
                    msg += deckName + ', '
                if len(list(data[ctx.author.id].keys())) != 0:
                    msg = msg[:-2]
                await ctx.send(msg)
            elif args[0] in list(data[ctx.author.id].keys()):
                msg = 'Cards in ' + args[0] + ': '
                for cardName in data[ctx.author.id][args[0]]:
                    msg += cardName.replace('_', ' ') + ' | '
                if len(data[ctx.author.id][args[0]]) != 0:
                    msg = msg[:-3]
                await ctx.send(msg)
            else:
                await ctx.send(args[0] + ' not found.')

        if changeFlag:
            f = open('decks.json', 'w')
            json.dump(data, f)
            f.close()


    @commands.command()
    async def discard(self, ctx, *args):
        '''PMs player information about their discard pile.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].discard(ctx)


    @commands.command()
    async def end(self, ctx, *args):
        '''Ends the player's turn.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].end(ctx, *args)


    @commands.command()
    async def explain(self, ctx, *args):
        '''Format: ~explain [card name, including spaces]
        i.e. ~explain the blank room
        Provides details about a card.'''
        entry = ' '.join(args).title()
        info = self.database.lookup(entry)
        if info is None:
            await ctx.send(entry + ' not found.')
            return
        msg = ''
        for key in list(info.keys()):
            msg += key.capitalize() + ': ' + str(info[key]) + '\n'
        await ctx.send(msg)


    @commands.command()
    async def field(self, ctx, *args):
        '''Shows the field of the current game.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].field(ctx, *args)


    @commands.command()
    async def flip(self, ctx, *args):
        '''Format: ~flip [# of dream] [optional: # of active on dream]
        i.e. ~flip 2 1, flips the first active on the second dream.
        Flip a card on the field, turning it face down or face up.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].flip(ctx, *args)


    @commands.command()
    async def hand(self, ctx, *args):
        '''PMs the user their current hand of cards.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].hand(ctx, *args)


    @commands.command()
    async def join(self, ctx, *args):
        '''Format: ~join [lane #]
        Joins a game.'''
        if ctx.author.id in list(registry.keys()):
            await ctx.send('You are already in Lane ' + str(registry[ctx.author.id].id) + '.')
            return
        if len(args) > 0 and args[0].isnumeric():
            if int(args[0]) <= len(self.lanes) and int(args[0]) > 0:
                lane = self.lanes[int(args[0]) - 1]
                if None in lane.players:
                    if lane.players[0] is None:
                        lane.players[0] = player.Player(ctx.author.display_name, ctx.author.id, ctx.author, lane.channel)
                    else:
                        lane.players[1] = player.Player(ctx.author.display_name, ctx.author.id, ctx.author, lane.channel)
                    registry[ctx.author.id] = lane
                    await ctx.send('Added ' + ctx.author.display_name + ' to Lane ' + str(lane.id) + '. Remember to set your deck with the ~set command.')
                else:
                    await ctx.send('Lane ' + str(lane.id) + 'is full.')
            else:
                await ctx.send('Could not find Lane ' + args[0] + '.')
        else:
            await ctx.send('Error: Improper format.')


    @commands.command()
    async def leave(self, ctx, *args):
        '''Leaves a game.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        if len(args) > 0:
            if args[0].isnumeric():
                if int(args[0]) <= len(self.lanes) and int(args[0]) > 0:
                    lane = self.lanes[int(args[0]) - 1]
                    if ctx.author.id in [p.id if p is not None else -1 for p in lane.players]:
                        lane.players = [None if p is not None and p.id == ctx.author.id else p for p in lane.players]
                        registry.pop(ctx.author.id, None)
                        await ctx.send('Removed ' + ctx.author.display_name + ' from Lane ' + str(lane.id) + '.')
                    else:
                        await ctx.send('Could not find ' + ctx.author.display_name + ' in Lane ' + str(lane.id) + '.')
                else:
                    await ctx.send('Could not find Lane ' + args[0] + '.')
            else:
                await ctx.send('Error: Improper format.')
        else:
            flag = True
            for lane in self.lanes:
                if ctx.author.id in [p.id if p is not None else -1 for p in lane.players]:
                    lane.players = [None if p is not None and p.id == ctx.author.id else p for p in lane.players]
                    registry.pop(ctx.author.id, None)
                    await ctx.send('Removed ' + ctx.author.display_name + ' from Lane ' + str(lane.id) + '.')
                    flag = False
            if flag:
                await ctx.send('Could not find ' + ctx.author.display_name + ' in any lanes.')


    @commands.command()
    async def new(self, ctx, *args):
        '''Sets up a new game.'''
        guild = ctx.message.guild
        cat = None
        for c in guild.categories:
            if c.name == 'Lanes':
                cat = c
        chanName = 'lane-' + str(len(self.lanes)+1)

        chan = None
        for c in cat.text_channels:
            if chanName == c.name:
                chan = c
        if chan is None:
            chan = await guild.create_text_channel(chanName, category=cat)
        lane = Lane(len(self.lanes)+1, None, None, chan)
        self.lanes.append(lane)
        await ctx.send('New lane created. ID: ' + str(lane.id))


    @commands.command()
    async def play(self, ctx, *args):
        '''Format: ~play [card name, including spaces] [d/u, face-down or face-up] [p/a, passive or active] [if active: # of dream to play the card on]
        i.e. ~play old gerard d a 1, plays Old Gerard face down on Dream 1.
        Plays a card onto the field.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].play(ctx, *args)


    @commands.command()
    async def reset(self, ctx, *args):
        '''Wipes deck data'''
        f = open('decks.json', 'w+')
        json.dump({}, f)
        f.close()
        await ctx.send("All decks reset.")


    @commands.command()
    async def set(self, ctx, *args):
        '''Format: ~set [deck name]
        i.e. ~set random
        Sets deck to use in game.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return

        f = open('decks.json', 'w+')
        if os.stat('decks.json').st_size > 0:
            data = json.load(f)
            f.close()
        else:
            data = {}
        changeFlag = False

        if ctx.author.id not in data.keys():
            changeFlag = True
            data[ctx.author.id] = {
                'random': random.choices([key.replace(' ', '_') for key in list(self.database.data.keys())], k=30),
            }
        
        if len(args) > 0:
            if ctx.author.id in registry:
                if args[0] in data[ctx.author.id]:
                    for player in registry[ctx.author.id].players:
                        if player is not None and player.id == ctx.author.id:
                            deck = []
                            module = __import__('cards')
                            for card in data[ctx.author.id][args[0]]:
                                _class = getattr(module, card)
                                deck.append(_class(player, registry[ctx.author.id].channel, registry[ctx.author.id]))
                            player.deck = deck
                            await ctx.send('Set ' + ctx.author.display_name + '\'s deck to ' + args[0] + '.')

        if changeFlag:
            f = open('decks.json', 'w+')
            json.dump(data, f)
            f.close()


    @commands.command()
    async def spend(self, ctx, *args):
        '''Format: ~spend [# of Value user gains] [# of Value opponent loses]
        i.e. ~spend 2 3, user gains 2 Value, opponent loses 3 Value
        Done when one gains Value at the start of their turn.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].spend(ctx, *args)


    @commands.command()
    async def start(self, ctx, *args):
        '''Starts the game.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].start(ctx, *args)


    @commands.command()
    async def status(self, ctx, *args):
        '''Game Infodump'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].status(ctx, *args)


    @commands.command()
    async def value(self, ctx, *args):
        '''PMs the user with their current Value.'''
        if ctx.author.id not in list(registry.keys()):
            await ctx.send('You are not in any Lanes.')
            return
        await registry[ctx.author.id].value(ctx, *args)


    @commands.command()
    async def view(self, ctx, *args):
        '''Shows all currently active games.'''
        msg = 'Current Lanes:\n'
        for lane in self.lanes:
            msg += 'Lane ' + str(lane.id) + ' - '
            for player in lane.players:
                if player is not None:
                    msg += player.name + ', '
                else:
                    msg += '[Vacant], '
            msg = msg[:-2]
            msg += '\n'
        if len(self.lanes) == 0:
            msg += 'No active lanes.'
        await ctx.send(msg)